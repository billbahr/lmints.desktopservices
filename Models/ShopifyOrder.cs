﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplify.Desktop.Ws
{
    class ShopifyOrder
    {
        public string OrderId { get; set; }
        public string OrderNumber { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
