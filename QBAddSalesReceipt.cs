﻿using System;
using System.IO;
using Interop.QBFC13;
using log4net;

namespace Simplify.Desktop.Ws
{
	/// <summary>
	///---------------------------------------------------------------------------
	/// SalesAdd
	///
	/// Description:  
	/// Add sales receipts to QB desktop
	/// 
	/// Created On: 3/25/2021
	///   Bill Bahr
	///
	/// Change Log: 
	/// 
	///---------------------------------------------------------------------------
	/// </summary>
	public class QBAddSalesReceipt
	{
		private readonly ILog _log = LogManager.GetLogger("");

		public QBAddSalesReceipt()
		{

		}

		// Code for handling different versions of QuickBooks
		private double QBFCLatestVersion(QBSessionManager SessionManager)
		{
			// Use oldest version to ensure that this application work with any QuickBooks (US)
			IMsgSetRequest msgset = SessionManager.CreateMsgSetRequest("US", 1, 0);
			msgset.AppendHostQueryRq();
			IMsgSetResponse QueryResponse = SessionManager.DoRequests(msgset);
			SaveXML(msgset.ToXMLString());


			// The response list contains only one response,
			// which corresponds to our single HostQuery request
			IResponse response = QueryResponse.ResponseList.GetAt(0);

			// Please refer to QBFC Developers Guide for details on why 
			// "as" clause was used to link this derrived class to its base class
			IHostRet HostResponse = response.Detail as IHostRet;
			IBSTRList supportedVersions = HostResponse.SupportedQBXMLVersionList as IBSTRList;

			int i;
			double vers;
			double LastVers = 0;
			string svers = null;

			for (i = 0; i <= supportedVersions.Count - 1; i++)
			{
				svers = supportedVersions.GetAt(i);
				vers = Convert.ToDouble(svers);
				if (vers > LastVers)
				{
					LastVers = vers;
				}
			}
			return LastVers;
		}


		public IMsgSetRequest getLatestMsgSetRequest(QBSessionManager sessionManager)
		{
			// Find and adapt to supported version of QuickBooks
			double supportedVersion = QBFCLatestVersion(sessionManager);

			short qbXMLMajorVer = 0;
			short qbXMLMinorVer = 0;

			if (supportedVersion >= 6.0)
			{
				qbXMLMajorVer = 6;
				qbXMLMinorVer = 0;
			}
			else if (supportedVersion >= 5.0)
			{
				qbXMLMajorVer = 5;
				qbXMLMinorVer = 0;
			}
			else if (supportedVersion >= 4.0)
			{
				qbXMLMajorVer = 4;
				qbXMLMinorVer = 0;
			}
			else if (supportedVersion >= 3.0)
			{
				qbXMLMajorVer = 3;
				qbXMLMinorVer = 0;
			}
			else if (supportedVersion >= 2.0)
			{
				qbXMLMajorVer = 2;
				qbXMLMinorVer = 0;
			}
			else if (supportedVersion >= 1.1)
			{
				qbXMLMajorVer = 1;
				qbXMLMinorVer = 1;
			}
			else
			{
				qbXMLMajorVer = 1;
				qbXMLMinorVer = 0;
			}

			// Create the message set request object
			IMsgSetRequest requestMsgSet = sessionManager.CreateMsgSetRequest("US", qbXMLMajorVer, qbXMLMinorVer);
			return requestMsgSet;
		}

		public void QBFC_AddSales()
		{
			IMsgSetRequest requestMsgSet;
			IMsgSetResponse responseMsgSet;
			// Create the session manager object using QBFC
			QBSessionManager sessionManager = new QBSessionManager();

			// We want to know if we begun a session so we can end it if an
			// error happens
			bool booSessionBegun = false;

			try
			{
				// Use SessionManager object to open a connection and begin a session 
				// with QuickBooks. At this time, you should add interop.QBFCxLib into 
				// your Project References
				sessionManager.OpenConnection("", "Add Simplify sales receipt");
				sessionManager.BeginSession("", ENOpenMode.omDontCare);
				booSessionBegun = true;

				// Get the RequestMsgSet based on the correct QB Version
				requestMsgSet = getLatestMsgSetRequest(sessionManager);

				// Initialize the message set request object
				requestMsgSet.Attributes.OnError = ENRqOnError.roeContinue;

				// ERROR RECOVERY: 
				// All steps are described in QBFC Developers Guide, on pg 41
				// under section titled "Automated Error Recovery"

				// (1) Set the error recovery ID using ErrorRecoveryID function
				//		Value must be in GUID format
				string errecid = Guid.NewGuid().ToString();
				sessionManager.ErrorRecoveryID.SetValue(errecid);

				// (2) Set EnableErrorRecovery to true to enable error recovery
				sessionManager.EnableErrorRecovery = true;

				// (3) Set SaveAllMsgSetRequestInfo to true so the entire contents of the MsgSetRequest
				//		will be saved to disk. If SaveAllMsgSetRequestInfo is false (default), only the 
				//		newMessageSetID will be saved. 
				sessionManager.SaveAllMsgSetRequestInfo = true;

				// (4) Use IsErrorRecoveryInfo to check whether an unprocessed response exists. 
				//		If IsErrorRecoveryInfo is true:
				if (sessionManager.IsErrorRecoveryInfo())
				{
					IMsgSetRequest reqMsgSet = null;
					IMsgSetResponse resMsgSet = null;

					// a. Get the response status, using GetErrorRecoveryStatus
					resMsgSet = sessionManager.GetErrorRecoveryStatus();
					// resXML = resMsgSet.ToXMLString();
					// MessageBox.Show(resXML);

					if (resMsgSet.Attributes.MessageSetStatusCode.Equals("600"))
					{
						// This case may occur when a transaction has failed after QB processed 
						// the request but client app didn't get the response and started with 
						// another company file.
						//MessageBox.Show("The oldMessageSetID does not match any stored IDs, and no newMessageSetID is provided.");
					}
					else if (resMsgSet.Attributes.MessageSetStatusCode.Equals("9001"))
					{
						//MessageBox.Show("Invalid checksum. The newMessageSetID specified, matches the currently stored ID, but checksum fails.");
					}
					else if (resMsgSet.Attributes.MessageSetStatusCode.Equals("9002"))
					{
						// Response was not successfully stored or stored properly
						//MessageBox.Show("No stored response was found.");
					}
					// 9003 = Not used
					else if (resMsgSet.Attributes.MessageSetStatusCode.Equals("9004"))
					{
						// MessageSetID is set with a string of size > 24 char
						//MessageBox.Show("Invalid MessageSetID, greater than 24 character was given.");
					}
					else if (resMsgSet.Attributes.MessageSetStatusCode.Equals("9005"))
					{
						//MessageBox.Show("Unable to store response.");
					}
					else
					{
						IResponse res = resMsgSet.ResponseList.GetAt(0);
						int sCode = res.StatusCode;
						//string sMessage = res.StatusMessage;
						//string sSeverity = res.StatusSeverity;
						//MessageBox.Show("StatusCode = " + sCode + "\n" + "StatusMessage = " + sMessage + "\n" + "StatusSeverity = " + sSeverity);

						if (sCode == 0)
						{
							//MessageBox.Show("Last request was processed and Invoice was added successfully!");
						}
						else if (sCode > 0)
						{
							//MessageBox.Show("There was a warning but last request was processed successfully!");
						}
						else
						{
							//MessageBox.Show("It seems that there was an error in processing last request");
							// b. Get the saved request, using GetSavedMsgSetRequest
							reqMsgSet = sessionManager.GetSavedMsgSetRequest();
							//reqXML = reqMsgSet.ToXMLString();
							//MessageBox.Show(reqXML);

							// c. Process the response, possibly using the saved request
							resMsgSet = sessionManager.DoRequests(reqMsgSet);
							IResponse resp = resMsgSet.ResponseList.GetAt(0);
							int statCode = resp.StatusCode;
							if (statCode == 0)
							{
								string resStr = null;
								ISalesReceiptRet slsRet = resp.Detail as ISalesReceiptRet;
								resStr = resStr + "Following sales receipt has been successfully submitted to QuickBooks:\n\n\n";
								if (slsRet.TxnNumber != null)
									resStr = resStr + "Txn Number = " + Convert.ToString(slsRet.TxnNumber.GetValue()) + "\n";
							} // if (statusCode == 0)
						} // else (sCode)
					} // else (MessageSetStatusCode)

					// d. Clear the response status, using ClearErrorRecovery
					sessionManager.ClearErrorRecovery();
				}


				// Add the request to the message set request object
				ISalesReceiptAdd SalesAdd = requestMsgSet.AppendSalesReceiptAddRq();


				// Set the ISalesAdd field values
				// Customer:Job
				string customer = "Acme Company";
				if (!customer.Equals(""))
				{
					SalesAdd.CustomerRef.FullName.SetValue(customer);
				}
				// Invoice Date
				string salesDate = "2021-03-17";
				if (!salesDate.Equals(""))
				{
					SalesAdd.TxnDate.SetValue(Convert.ToDateTime(salesDate));
				}
				// Invoice Number
				string invoiceNumber = "TEST1";
				if (!invoiceNumber.Equals(""))
				{
					SalesAdd.RefNumber.SetValue(invoiceNumber);
				}
				// Bill Address
				string bAddr1 = "123 Main St";
				string bAddr2 = "";
				string bAddr3 = "";
				string bAddr4 = "";
				string bCity = "City";
				string bState = "MN";
				string bPostal = "55555";
				string bCountry = "";
				SalesAdd.BillAddress.Addr1.SetValue(bAddr1);
				SalesAdd.BillAddress.Addr2.SetValue(bAddr2);
				SalesAdd.BillAddress.Addr3.SetValue(bAddr3);
				SalesAdd.BillAddress.Addr4.SetValue(bAddr4);
				SalesAdd.BillAddress.City.SetValue(bCity);
				SalesAdd.BillAddress.State.SetValue(bState);
				SalesAdd.BillAddress.PostalCode.SetValue(bPostal);
				SalesAdd.BillAddress.Country.SetValue(bCountry);

				// Customer Message
				string customerMsg = "";
				if (!customerMsg.Equals(""))
				{
					SalesAdd.CustomerMsgRef.FullName.SetValue(customerMsg);
				}

				// Set the values for the invoice line
				string item = "123456";
				string desc = "";
				string rate = "10.00";
				string qty = "4.0";
				string amount = "40.00";

				if (!item.Equals("") || !desc.Equals(""))
				{
					ISalesReceiptLineAdd salesLineAdd = SalesAdd.ORSalesReceiptLineAddList.Append().SalesReceiptLineAdd;
					salesLineAdd.ItemRef.FullName.SetValue(item);
					salesLineAdd.Desc.SetValue(desc);
					salesLineAdd.ORRatePriceLevel.Rate.SetValue(Convert.ToDouble(rate));
					salesLineAdd.Quantity.SetValue(Convert.ToDouble(qty));
					salesLineAdd.Amount.SetValue(Convert.ToDouble(amount));
				} // for

				// Uncomment the following to view and save the request and response XML
				string requestXML = requestMsgSet.ToXMLString();
				SaveXML(requestXML);

				// Perform the request and obtain a response from QuickBooks			
				responseMsgSet = sessionManager.DoRequests(requestMsgSet);

				// Uncomment the following to view and save the request and response XML
				requestXML = requestMsgSet.ToXMLString();
				SaveXML(requestXML);
				string responseXML = responseMsgSet.ToXMLString();
				SaveXML(responseXML);

				/* IResponse response = responseMsgSet.ResponseList.GetAt(0);
				int statusCode = response.StatusCode;
				// string statusMessage = response.StatusMessage;
				// string statusSeverity = response.StatusSeverity;
				// MessageBox.Show("Status:\nCode = " + statusCode + "\nMessage = " + statusMessage + "\nSeverity = " + statusSeverity);

				if (statusCode == 0)
				{
					string resString = null;
					IInvoiceRet invoiceRet = response.Detail as IInvoiceRet;
					resString = resString + "Following invoice has been successfully submitted to QuickBooks:\n\n\n";
					if (invoiceRet.TimeCreated != null)
						resString = resString + "Time Created = " + Convert.ToString(invoiceRet.TimeCreated.GetValue()) + "\n";
					if (invoiceRet.TxnNumber != null)
						resString = resString + "Txn Number = " + Convert.ToString(invoiceRet.TxnNumber.GetValue()) + "\n";
					if (invoiceRet.TxnDate != null)
						resString = resString + "Txn Date = " + Convert.ToString(invoiceRet.TxnDate.GetValue()) + "\n";
					if (invoiceRet.RefNumber != null)
						resString = resString + "Reference Number = " + invoiceRet.RefNumber.GetValue() + "\n";
					if (invoiceRet.CustomerRef.FullName != null)
						resString = resString + "Customer FullName = " + invoiceRet.CustomerRef.FullName.GetValue() + "\n";
					resString = resString + "\nBilling Address:" + "\n";
					if (invoiceRet.BillAddress.Addr1 != null)
						resString = resString + "Addr1 = " + invoiceRet.BillAddress.Addr1.GetValue() + "\n";
					if (invoiceRet.BillAddress.Addr2 != null)
						resString = resString + "Addr2 = " + invoiceRet.BillAddress.Addr2.GetValue() + "\n";
					if (invoiceRet.BillAddress.Addr3 != null)
						resString = resString + "Addr3 = " + invoiceRet.BillAddress.Addr3.GetValue() + "\n";
					if (invoiceRet.BillAddress.Addr4 != null)
						resString = resString + "Addr4 = " + invoiceRet.BillAddress.Addr4.GetValue() + "\n";
					if (invoiceRet.BillAddress.City != null)
							resString = resString + "City = " + invoiceRet.BillAddress.City.GetValue() + "\n";
					if (invoiceRet.BillAddress.State != null)
						resString = resString + "State = " + invoiceRet.BillAddress.State.GetValue() + "\n";
					if (invoiceRet.BillAddress.PostalCode != null)
						resString = resString + "Postal Code = " + invoiceRet.BillAddress.PostalCode.GetValue() + "\n";
					if (invoiceRet.BillAddress.Country != null)
						resString = resString + "Country = " + invoiceRet.BillAddress.Country.GetValue() + "\n";
					if (invoiceRet.PONumber != null)
						resString = resString + "\nPO Number = " + invoiceRet.PONumber.GetValue() + "\n";
					if (invoiceRet.TermsRef.FullName != null)
						resString = resString + "Terms = " + invoiceRet.TermsRef.FullName.GetValue() + "\n";
					if (invoiceRet.DueDate != null)
						resString = resString + "Due Date = " + Convert.ToString(invoiceRet.DueDate.GetValue()) + "\n";
					if (invoiceRet.SalesTaxTotal != null)
						resString = resString + "Sales Tax = " + Convert.ToString(invoiceRet.SalesTaxTotal.GetValue()) + "\n";
					resString = resString + "\nInvoice Line Items:" + "\n";
					IORInvoiceLineRetList orInvoiceLineRetList = invoiceRet.ORInvoiceLineRetList;
					string fullname = "<empty>";
					string desc = "<empty>";
					string rate = "<empty>";
					string quantity = "<empty>";
					string amount = "<empty>";
					for (int i = 0; i <= orInvoiceLineRetList.Count - 1; i++)
					{
						if (invoiceRet.ORInvoiceLineRetList.GetAt(i).ortype == ENORInvoiceLineRet.orilrInvoiceLineRet)
						{
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.ItemRef.FullName != null)
								fullname = invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.ItemRef.FullName.GetValue();
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.Desc != null)
								desc = invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.Desc.GetValue();
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.ORRate.Rate != null)
								rate = Convert.ToString(invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.ORRate.Rate.GetValue());
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.Quantity != null)
								quantity = Convert.ToString(invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.Quantity.GetValue());
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.Amount != null)
								amount = Convert.ToString(invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineRet.Amount.GetValue());
						}
						else
						{
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.ItemGroupRef.FullName != null)
								fullname = invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.ItemGroupRef.FullName.GetValue();
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.Desc != null)
								desc = invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.Desc.GetValue();
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.InvoiceLineRetList.GetAt(i).ORRate.Rate != null)
								rate = Convert.ToString(invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.InvoiceLineRetList.GetAt(i).ORRate.Rate.GetValue());
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.Quantity != null)
								quantity = Convert.ToString(invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.Quantity.GetValue());
							if (invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.TotalAmount != null)
								amount = Convert.ToString(invoiceRet.ORInvoiceLineRetList.GetAt(i).InvoiceLineGroupRet.TotalAmount.GetValue());
						}
						resString = resString + "Fullname: " + fullname + "\n";
						resString = resString + "Description: " + desc + "\n";
						resString = resString + "Rate: " + rate + "\n";
						resString = resString + "Quantity: " + quantity + "\n";
						resString = resString + "Amount: " + amount + "\n\n";
					}
					MessageBox.Show(resString);
				} // if statusCode is zero */

				// Close the session and connection with QuickBooks
				sessionManager.ClearErrorRecovery();
				sessionManager.EndSession();
				booSessionBegun = false;
				sessionManager.CloseConnection();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message.ToString() + "\nStack Trace: \n" + ex.StackTrace + "\nExiting the application");
				if (booSessionBegun)
				{
					sessionManager.EndSession();
					sessionManager.CloseConnection();
				}

			}

		} // QBFC_AddSales

		void SaveXML(string xmlstring)
		{
			string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			StreamWriter sr = new StreamWriter(Path.Combine(docPath,"SavedXML.xml"),true);
			sr.Write(xmlstring);
			sr.Close();
		}


	} // class QBAddSalesReceipt
}
