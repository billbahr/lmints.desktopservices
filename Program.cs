﻿using System.ServiceProcess;

namespace Simplify.Desktop.Ws
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new SimplifyDesktopService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
