﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System.Timers;

namespace Simplify.Desktop.Ws
{
    [RunInstaller(true)]
    public partial class SimplifyDesktopService : ServiceBase
    {
        FileSystemWatcher fileWatcher;

        public SimplifyDesktopService()
        {
            InitializeComponent();
            int wait = int.Parse(ConfigurationManager.AppSettings["timerInterval"]) * 1000;
            timer = new Timer(wait);
            timer.Elapsed += timer_Elapsed;

            // We don't want the timer to start ticking again till we tell it to.
            timer.AutoReset = false;
        }

        private System.Timers.Timer timer;
        private string inPath = @"C:\Users\billb\source\in";
        private string tempPath = @"C:\Users\billb\source\temp\";
        private string serviceLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        private string transType = ConfigurationManager.AppSettings["transType"];


        protected override void OnStart(string[] args)
        {

            timer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {

            try
            {
                // check for input files and move to temp
                var inputFiles = Directory.EnumerateFiles(inPath, "*.xml");
                foreach (var xFile in inputFiles)
                {

                    try
                    {
                        File.Move(xFile, tempPath + Path.GetFileName(xFile));
                    }
                    catch (Exception ex)
                    {
                        var msg = $"Error moving {xFile} {ex} {System.Environment.NewLine}";
                        File.AppendAllText($"{serviceLocation}\\watchlog.txt", msg);
                        throw;
                    }
                }

                // process all files in temp and move to processed
                var tempFiles = Directory.EnumerateFiles(tempPath, "*.xml");
                foreach (string xFile in tempFiles)
                {
                    if (transType == "invoice")
                    {
                        var msg = $"Create QB invoice from {xFile} {System.Environment.NewLine}";
                        File.AppendAllText($"{serviceLocation}\\watchlog.txt", msg);
                        QBAddInvoice newInvoice = new QBAddInvoice();
                        newInvoice.QBFC_AddInvoice(xFile);
                    }
                    else
                    {
                        var msg = $"Create QB sales receipt from {xFile} {System.Environment.NewLine}";
                        File.AppendAllText($"{serviceLocation}\\watchlog.txt", msg);
                    }
                }

                timer.Start();
            }
            catch (Exception ex)
            {
                var msg = $"Error checking input files {ex} {System.Environment.NewLine}";
                File.AppendAllText($"{serviceLocation}\\watchlog.txt", msg);
                timer.Start(); //Start the timer for the next loop
            }
        }

        protected override void OnStop()
        {
            timer.Stop();
        }
    }
}
